#### 4. Crea una imagen docker a partir de un Dockerfile. Esta aplicación expondrá un servicio en el puerto 8080 y se deberá hacer uso de la instrucción HEALTHCHECK para comprobar si la aplicación está ofreciendo el servicio o por si el contrario existe un problema.

     a. Generación de Dockerfile
     b. Generación de Imagen “docker build -t health-check .”
     c. Ejecución de contenedor “docker run --rm -it -d -p 8080:8080 health-check”

Dockerfile
```dockerfile
FROM node:lts-alpine3.14    
WORKDIR /usr/src/app
COPY ./service/package*.json ./
RUN npm install
COPY ./service .
EXPOSE 8080

HEALTHCHECK --interval=45s --timeout=5s --start-period=15s --retries=2 CMD wget --no-verbose --tries=1 --spider http://localhost:8080/health || exit 1

ENTRYPOINT ["node","index.js"]
```

Resultado

![](images/02.png)

![](images/03.png)
