#### 5. “Proof of Concept” (PoC).
        a. Generar docker-compose.yml”
        b. Ejecutar compose “docker-compose up -d”

docker-compose.yml
```yaml
version: '3.6'
services:

  elasticsearch:
    # Utilizar la imagen de elasticsearch v7.9.3
    image: docker.elastic.co/elasticsearch/elasticsearch:7.9.3
    # Asignar un nombre al contenedor
    container_name: elasticsearch
    # Define las siguientes variables de entorno:
    environment:
    # discovery.type=single-node
    - discovery.type=single-node
    # Emplazar el contenedor a la red de elastic    
    networks:
      - elastic-network    
    ports:
    # Mapea el Puerto externo 9200 al puerto interno del contenedor 9200
    - 9200:9200
    # Idem para el puerto 9300
    - 9300:9300    

  kibana:
    # Utilizar la imagen kibana v7.9.3
    image: docker.elastic.co/kibana/kibana:7.9.3
    # Asignar un nombre al contenedor
    container_name: kibana
    # Emplazar el contenedor a la red de elastic
    networks:
      - elastic-network 
    # Define las siguientes variables de entorno:
    environment:
    # ELASTICSEARCH_HOST=elasticsearch
    - ELASTICSEARCH_HOST=elasticsearch
    # ELASTICSEARCH_PORT=9200
    - ELASTICSEARCH_PORT=9200
    ports:
      # Mapear el puerto externo 5601 al puerto interno 5601
      - 5601:5601
      # El contenedor Kibana depe esperar a la disponibilidad del servicio elasticsearch
    depends_on:
      - elasticsearch
# Definir la red elastic (bridge)
networks:
  elastic-network:
    driver: bridge
```
