#### 3. Crea un contenedor con las siguientes especificaciones:

     a. Utilizar la imagen base NGINX haciendo uso de la versión 1.19.3
        - Se creo un Dockerfile basado en la imagen indicada.      
![](images/01.png)

        - Comando de compilación “docker build -t nginx-1.19.3 .”
![](images/02.png)

        - Comando de ejecución “docker run --rm -it -p 8080:80 nginx-1.19.3”
![](images/03.png)
 
     b. Al acceder a la URL localhost:8080/index.html aparecerá el mensaje HOMEWORK 1
        - Se agrego el comando COPY, para sobreescribir el index.html
![](images/04.png)

![](images/05.png)

     c. Persistir el fichero index.html en un volumen llamado static_content
        - Comando de creación de volumen “docker volume create static_content”
        - Comando de listado de volúmenes “docker volume list”
![](images/06.png)

        - Comando de asignación de volumen “docker run --rm -it -p 8080:80 -v static_content:/usr/share/nginx/html nginx-1.19.3”
