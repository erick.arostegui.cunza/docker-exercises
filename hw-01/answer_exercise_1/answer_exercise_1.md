#### 1. Diferencia entre el uso de la instrucción CMD y ENTRYPOINT

La diferencia principal es que el CMD acepta parámetros de ejecución desde la línea de comando lo cual puede hacerlo susceptible a ataques, y ENTRYPOINT acepta parámetros predeterminados que no se pueden cambiar desde la línea de comandos.
