const express = require("express");
const app = express();
app.get("/health", (req, res) => {
  res.send({ success: true, message: "It is working" });
});
app.get("/", (req, res) => {
  res.send({ success: true, message: "HOMEWORK 1" });
});
const PORT = process.env.PORT || 8080;
app.listen(PORT, () => {
  console.log(`Server is running on PORT ${PORT}`);
});